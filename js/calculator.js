$(document).ready(function() {
  // Kalk Vue
  const app = new Vue({
    el: "#app",
    data: {
      mwhPerYear: 0,
      roofArea: 0,
      panelType: "panel-roof",
      roofAngle: 30,
      roofDirection: 0,
      isRoof: true,
      isWall: false,
      isGround: false,
      latitudeClass: ""
    },
    watch: {
      panelType(newVal, oldVal) {
        if (newVal === "panel-ground") {
          this.roofAngle = 30;
          this.isRoof = false;
          this.isGround = true;
          this.isWall = false;
        } else if (newVal === "panel-wall") {
          this.roofAngle = 90;
          this.isRoof = false;
          this.isGround = false;
          this.isWall = true;
        } else if (newVal === "panel-roof") {
          this.isRoof = true;
          this.isGround = false;
          this.isWall = false;
        }
      }
    },
    computed: {
      panelAmount() {
        return this.mwhPerYear > 0
          ? Math.ceil(this.mwhPerYear / (275 * this.panelValue))
          : 0;
      },
      yearSavings() {
        return Math.ceil(this.mwhPerYear * 1.25);
      },
      investmentCost() {
        return this.panelType === "panel-ground"
          ? Math.ceil(this.mwhPerYear * 8.7)
          : Math.ceil(this.mwhPerYear * 9.7);
      },
      paymentTime() {
        return this.investmentCost && this.yearSavings
          ? Math.ceil(this.investmentCost / this.yearSavings)
          : 0;
      },
      maxAmountPanels() {
        return Math.floor(this.roofArea / 1.7);
      },
      morePanelsNeeded() {
        return this.roofArea && this.panelAmount > this.maxAmountPanels
          ? true
          : false;
      },
      latitudeClassObject() {
        const lat = this.latitudeClass;
        return {
          east: lat === "east",
          southeast: lat === "southeast",
          south: lat === "south",
          southwest: lat === "southwest",
          west: lat === "west"
        };
      },
      latitude() {
        const direction = this.roofDirection;
        switch (true) {
          case direction < -40:
            this.latitudeClass = "east";
            return "Öst";
          case direction >= -50 && direction <= -20:
            this.latitudeClass = "southeast";
            return "Sydöst";
          case direction >= -10 && direction <= -20:
            this.latitudeClass = "southeast";
            return "Sydöst";
          case direction >= -10 && direction <= 10:
            this.latitudeClass = "south";
            return "Syd";
          case direction >= 20 && direction <= 50:
            this.latitudeClass = "southwest";
            return "Sydväst";
          default:
            this.latitudeClass = "west";
            return "Väst";
        }
      },
      // Calculates the value based on angle and direction
      panelValue() {
        let value;
        // Take provided angle and check the angles object for that key.
        const angleArray = angles[this.roofAngle];
        // Loop through the directions and get the index based on provided direction.
        directions.map((val, index) => {
          if (val == this.roofDirection) {
            // Polulate the empty calcValue with the data from the index value in the angleArray
            value = angleArray[index] / 100;
          }
        });
        return value;
      }
    }
  });

  // Ensure that only digits work on inputs
  $("input.only-digits").keydown(function(e) {
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39) ||
      // Allow ctrl v
      (e.keyCode === 86 && e.ctrlKey === true) ||
      // Allow ctrl c
      (e.keyCode === 67 && e.ctrlKey === true) ||
      // Allow ctrl x
      (e.keyCode === 88 && e.ctrlKey === true)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Test that current key is digits
    if (/^[0-9]+$/.test(e.key)) {
      // If it is a digit, return
      return;
    } else {
      // If its not a digit, prevent.
      e.preventDefault();
    }
  });
});

var angles = {
  "90": [
    57,
    62,
    66,
    70,
    73,
    76,
    78,
    79,
    80,
    80,
    80,
    79,
    77,
    75,
    73,
    69,
    65,
    61,
    57
  ],
  "85": [
    61,
    66,
    70,
    74,
    78,
    80,
    83,
    84,
    85,
    85,
    85,
    83,
    83,
    80,
    77,
    73,
    69,
    65,
    61
  ],
  "80": [
    63,
    69,
    74,
    78,
    82,
    85,
    87,
    88,
    89,
    89,
    89,
    88,
    86,
    84,
    81,
    77,
    73,
    68,
    63
  ],
  "75": [
    68,
    73,
    77,
    82,
    85,
    88,
    90,
    92,
    93,
    93,
    93,
    91,
    90,
    87,
    84,
    80,
    76,
    71,
    66
  ],
  "70": [
    70,
    76,
    80,
    85,
    88,
    91,
    93,
    95,
    96,
    96,
    96,
    93,
    93,
    90,
    87,
    84,
    79,
    75,
    70
  ],
  "65": [
    73,
    78,
    83,
    87,
    91,
    94,
    95,
    97,
    98,
    98,
    98,
    95,
    93,
    91,
    90,
    86,
    82,
    77,
    72
  ],
  "60": [
    76,
    81,
    85,
    90,
    93,
    95,
    97,
    98,
    98,
    98,
    98,
    97,
    93,
    93,
    92,
    89,
    84,
    80,
    75
  ],
  "55": [
    78,
    83,
    87,
    92,
    95,
    96,
    98,
    99,
    99,
    99,
    99,
    98,
    97,
    94,
    93,
    90,
    86,
    82,
    77
  ],
  "50": [
    80,
    85,
    89,
    93,
    96,
    97,
    98,
    100,
    100,
    100,
    100,
    100,
    97,
    96,
    94,
    91,
    88,
    84,
    79
  ],
  "45": [
    81,
    86,
    90,
    94,
    96,
    98,
    99,
    100,
    100,
    100,
    100,
    100,
    98,
    97,
    95,
    92,
    89,
    85,
    80
  ],
  "40": [
    83,
    87,
    91,
    95,
    96,
    98,
    99,
    100,
    100,
    100,
    100,
    100,
    98,
    97,
    95,
    92,
    90,
    86,
    82
  ],
  "35": [
    84,
    88,
    92,
    95,
    96,
    98,
    99,
    100,
    100,
    100,
    100,
    100,
    98,
    97,
    95,
    92,
    91,
    87,
    83
  ],
  "30": [
    85,
    88,
    92,
    95,
    95,
    98,
    98,
    99,
    99,
    99,
    99,
    98,
    97,
    96,
    94,
    92,
    91,
    88,
    84
  ],
  "25": [
    85,
    88,
    91,
    94,
    95,
    97,
    96,
    98,
    98,
    98,
    98,
    97,
    97,
    91,
    93,
    93,
    91,
    88,
    85
  ],
  "20": [
    86,
    88,
    91,
    93,
    90,
    95,
    95,
    97,
    97,
    97,
    97,
    95,
    95,
    90,
    93,
    91,
    91,
    88,
    85
  ],
  "15": [
    86,
    88,
    90,
    92,
    90,
    95,
    95,
    96,
    93,
    93,
    93,
    94,
    94,
    92,
    91,
    90,
    90,
    88,
    86
  ],
  "10": [
    86,
    88,
    89,
    91,
    90,
    93,
    92,
    93,
    93,
    93,
    93,
    93,
    93,
    93,
    90,
    90,
    89,
    88,
    86
  ],
  "5": [
    87,
    87,
    88,
    89,
    89,
    90,
    90,
    90,
    90,
    90,
    90,
    90,
    90,
    90,
    89,
    89,
    88,
    87,
    86
  ],
  "0": [
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87,
    87
  ]
};

var directions = [
  "-90",
  "-80",
  "-70",
  "-60",
  "-50",
  "-40",
  "-30",
  "-20",
  "-10",
  "0",
  "10",
  "20",
  "30",
  "40",
  "50",
  "60",
  "70",
  "80",
  "90"
];

$(document).ready(function() {
  $('.js-get-results').click(function(){
    $('.calc-right-half').addClass('active');
  });

  $('.js-close-results').click(function(){
    $('.calc-right-half').removeClass('active');
  });

  $('.tooltip').click(function(){
    $('.modal, .modal-overlay').fadeIn().css('display', 'flex');
  });

  $('.js-close-modal').click(function(){
    $('.modal, .modal-overlay').fadeOut();
  });

  $('.modal-overlay').click(function(){
    if(('.modal').hasClass('active')){
      $('.modal, .modal-overlay').removeClass('active');
    }
    else{

    }
  });

});
